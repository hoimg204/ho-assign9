//
//  Student.m
//  ho-assign9
//
//  Created by Henrique de Oliveira Carvalho on 2014-11-04.
//  Copyright (c) 2014 beta. All rights reserved.
//
//  Problem Statement 2:
//  Design and implement a Student class that contains name, age, and an
//  NSMutableArray of grades (NSNumber). Your task is to implement the
//  NSCopying protocol  (Deep Copy) for the Student class. Be sure that
//  the run method in Driver2 carries out the following tasks:
//  Create a Student object john with all instance variables properly
//  initialized (the array should contain several grades). Create another
//  Student object alice by copying john object with the mutableCopy
//  method (it means that you have to implement mutableCopyWithZone
//  method). Verify that both alice and john are objects that can be
//  modified without affecting each other.
//
//  Inputs: none
//  Outputs: none
// ************************************************************************

#import "Student.h"

@implementation Student

- (id) copyWithZone:(NSZone *)zone
{

  Student * copy = [[[self class] alloc] initWithName:_name andAge:_age];

  for (int pos = 0; pos < [_grades count]; pos++)
  {
    [[copy grades] addObject:_grades[pos]];
  }

  return copy;
}

- (id) initWithName:(NSString *)strName andAge:(int)iAge
{
  self = [super init];
  if (self)
  {
    _name = strName;
    _age = iAge;
    _grades = [[NSMutableArray alloc] init];
  }
  return self;
}

- (void) addGrade:(int)grade
{
  NSNumber * mygrade = [[NSNumber alloc] initWithInt:grade];

  [_grades addObject:mygrade];
}

- (int) numberOfGrades
{
  return (int) [_grades count];
}

- (NSString *) description
{

  NSString * str = @" ";

  str = [str stringByAppendingFormat:@"Student name is %@ ", _name];
  str = [str stringByAppendingFormat:@"and is %d years old\n", _age];

  for (int pos = 0; pos < [_grades count]; pos++)
  {
    int grade = (int) [_grades[pos] integerValue];
    str = [str stringByAppendingFormat:@"%d\n", grade];
  }

  str = [str
         stringByAppendingFormat:@"Number of grades: %d\n", [self numberOfGrades]];

  return str;
}

@end
