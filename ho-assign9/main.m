//
//  main.m
//  ho-assign9
//
//  Created by Henrique de Oliveira Carvalho on 2014-11-04.
//  Copyright (c) 2014 beta. All rights reserved.
//
//  Problem Statement 1:
//  Design an Objective-C protocol called Lockable that includes the
//  following methods: setKey, lock, unlock, and locked. The setKey,
//  lock, and unlock methods take an integer parameter that represents
//  the key. The setKey method establishes the key. The lock and unlock
//  methods lock and unlock the object, but only if the key passed in is
//  correct. The locked method returns a boolean that indicates whether
//  or not the object is locked. A Lockable object represents an object
//  whose regular methods are protected: if the object is locked, the
//  methods cannot be invoked; if it is unlocked, they can be invoked.
//  Redesign and implement a version of the Coin class in your Java
//  textbook (Chapter 5) so that it is Lockable.
//
//  INPUTS:    none
//  OUTPUTS:   none
//
// ************************************************************************
//
//  Problem Statement 2:
//  Design and implement a Student class that contains name, age, and an
//  NSMutableArray of grades (NSNumber). Your task is to implement the
//  NSCopying protocol  (Deep Copy) for the Student class. Be sure that
//  the run method in Driver2 carries out the following tasks:
//  Create a Student object john with all instance variables properly
//  initialized (the array should contain several grades). Create another
//  Student object alice by copying john object with the mutableCopy
//  method (it means that you have to implement mutableCopyWithZone
//  method). Verify that both alice and john are objects that can be
//  modified without affecting each other.
//
//  Inputs: none
//  Outputs: none
// ************************************************************************

#import <Foundation/Foundation.h>
#import "Driver1.h"
#import "Driver2.h"

int main(int argc, const char * argv[])
{
  @autoreleasepool {
    Driver1 * coin = [[Driver1 alloc] init];
    [coin run];

    Driver2 * student = [[Driver2 alloc] init];
    [student run];
  }
  return 0;
}
