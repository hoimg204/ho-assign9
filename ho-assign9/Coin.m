//
//  Coin.m
//  ho-assign9
//
//  Created by Henrique de Oliveira Carvalho on 2014-11-04.
//  Copyright (c) 2014 beta. All rights reserved.
//
//  Problem Statement 1:
//  Design an Objective-C protocol called Lockable that includes the
//  following methods: setKey, lock, unlock, and locked. The setKey,
//  lock, and unlock methods take an integer parameter that represents
//  the key. The setKey method establishes the key. The lock and unlock
//  methods lock and unlock the object, but only if the key passed in is
//  correct. The locked method returns a boolean that indicates whether
//  or not the object is locked. A Lockable object represents an object
//  whose regular methods are protected: if the object is locked, the
//  methods cannot be invoked; if it is unlocked, they can be invoked.
//  Redesign and implement a version of the Coin class in your Java
//  textbook (Chapter 5) so that it is Lockable.
//
//  INPUTS:    none
//  OUTPUTS:   none
//
// ***********************************************************************

#import "Coin.h"

@implementation Coin
@synthesize iFace, iKey, locked;

- (id) init
{
  self = [super init];

  if (self)
  {
    locked = NO;
    [self flip];
  }

  return self;
}

- (void) flip
{
  iFace = arc4random() % 2;
}

- (int) isHeads
{
  if (locked == NO)
  {
    if (iFace == 0)
    {
      return HEADS;
    }
    else
    {
      return TAILS;
    }
  }
  else
  {
    return LOCK;
  }
}

- (NSString *) description
{
  NSString * str;

  if (locked == NO)
  {
    if (iFace == 0)
    {
      str = @"Heads";
    }
    else
    {
      str = @"Tails";
    }
  }
  else
  {
    str = @"Coin locked";
  }

  return str;
}

- (void) setKey:(int)newKey
{
  if (locked == false)
  {
    iKey = newKey;
  }
}

- (void) lock:(int)newKey
{
  if (iKey == newKey)
  {
    locked = true;
  }
}

- (void) unlock:(int)newKey
{
  if (iKey == newKey)
  {
    locked = false;
  }
}

- (BOOL) locked
{
  return locked;
}

@end
