//
//  Driver1.h
//  ho-assign9
//
//  Created by Henrique de Oliveira Carvalho on 2014-11-04.
//  Copyright (c) 2014 beta. All rights reserved.
//
//  Problem Statement 1:
//  Design an Objective-C protocol called Lockable that includes the
//  following methods: setKey, lock, unlock, and locked. The setKey,
//  lock, and unlock methods take an integer parameter that represents
//  the key. The setKey method establishes the key. The lock and unlock
//  methods lock and unlock the object, but only if the key passed in is
//  correct. The locked method returns a boolean that indicates whether
//  or not the object is locked. A Lockable object represents an object
//  whose regular methods are protected: if the object is locked, the
//  methods cannot be invoked; if it is unlocked, they can be invoked.
//  Redesign and implement a version of the Coin class in your Java
//  textbook (Chapter 5) so that it is Lockable.
//
//  INPUTS:    none
//  OUTPUTS:   none
//
// ************************************************************************

#import <Foundation/Foundation.h>

@interface Driver1 : NSObject
- (void) run;
@end
