//
//  Driver2.m
//  ho-assign9
//
//  Created by Henrique de Oliveira Carvalho on 2014-11-04.
//  Copyright (c) 2014 beta. All rights reserved.
//
//  Problem Statement 2:
//  Design and implement a Student class that contains name, age, and an
//  NSMutableArray of grades (NSNumber). Your task is to implement the
//  NSCopying protocol  (Deep Copy) for the Student class. Be sure that
//  the run method in Driver2 carries out the following tasks:
//  Create a Student object john with all instance variables properly
//  initialized (the array should contain several grades). Create another
//  Student object alice by copying john object with the mutableCopy
//  method (it means that you have to implement mutableCopyWithZone
//  method). Verify that both alice and john are objects that can be
//  modified without affecting each other.
//
//  Inputs: none
//  Outputs: none
// ************************************************************************

#import "Driver2.h"
#import "Student.h"

@implementation Driver2

- (void) run
{
  Student * john = [[Student alloc] initWithName:@"john" andAge:13];

  [john addGrade:10];
  [john addGrade:20];
  [john addGrade:30];

  // Copying
  Student * alice = [john copy];
  [alice setName:@"alice"];

  // Testing diferent grades
  [john addGrade:40];
  [alice addGrade:50];

  NSLog(@"John: %@", john);
  NSLog(@"Alice: %@", alice);
}

@end
